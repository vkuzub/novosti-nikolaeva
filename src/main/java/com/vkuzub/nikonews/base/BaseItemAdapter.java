package com.vkuzub.nikonews.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by Vyacheslav on 26.07.2014.
 */
public class BaseItemAdapter extends BaseAdapter {

    protected Context ctx;                       //нужен для получения инфлятора
    protected LayoutInflater lInflater;          //инфлятор, нужен в подклассах для получения View из layour-xml в методе getView()
    protected List<? extends BaseItem> items;    //элементы для заполнения

    public BaseItemAdapter(Context ctx, List<? extends BaseItem> items) {
        this.ctx = ctx;
        this.items = items;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
