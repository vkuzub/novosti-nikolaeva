package com.vkuzub.nikonews.base;

/**
 * Created by Vyacheslav on 26.07.2014.
 */

import java.io.Serializable;

/**
 * Класс содержит базовую информацию о пункте списка (новость, статья, блог)
 */

public class BaseItem implements Serializable {

    public enum TYPES {
        NEWSPN, SITE0512
    }

    private String name;                         //Название новости
    private String time;                         //Время публикации
    private String url;                          //URL новости
    private TYPES type;                          //Тип сайта

    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public String getUrl() {
        return url;
    }

    public BaseItem(String name, String time, String url, TYPES type) {
        this.name = name;
        this.time = time;
        this.url = url;
        this.type = type;
    }

    public TYPES getType() {
        return type;
    }

    @Override
    public String toString() {
        return "BaseItem{" +
                "name='" + name + '\'' +
                ", time='" + time + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
