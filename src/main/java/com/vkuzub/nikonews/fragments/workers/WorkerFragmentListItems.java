package com.vkuzub.nikonews.fragments.workers;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;
import com.vkuzub.nikonews.R;
import com.vkuzub.nikonews.base.BaseItem;
import com.vkuzub.nikonews.parser.JsoupHtmlParser;
import com.vkuzub.nikonews.utils.NikoNewsUtils;

import java.io.IOException;
import java.util.List;

/**
 * Класс сохраняет статьи в кэш
 */
public class WorkerFragmentListItems extends Fragment {

    private TaskCallbacks callbacks;
    private Task task;

    public WorkerFragmentListItems(TaskCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        String articleUrl = getArguments().getString(NikoNewsUtils.INTENT_URL_EXTRA);
        char charKey = getArguments().getChar(NikoNewsUtils.INTENT_PARSER_KEY_EXTRA);
        task = new Task(charKey);
        task.execute(articleUrl);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    private class Task extends AsyncTask<String, Void, List<? extends BaseItem>> {

        private char type;

        private Task(char type) {
            this.type = type;
        }

        @Override
        protected List<? extends BaseItem> doInBackground(String... strings) {
            Log.d("MyLogs", "parser" + type);
            Log.d("MyLogs", "url:" + strings[0]);
            List<? extends BaseItem> items = null;
            try {
                switch (type) {
                    case '0':
                        items = JsoupHtmlParser.Site0512.parseNewsListFor0512(strings[0]);
                        break;
                    case 'n':
                        items = JsoupHtmlParser.PN.parseNewsListForPN(strings[0]);
                        break;
                }
            } catch (IOException e) {
                //TODO баги 0512
//                Toast.makeText(getActivity(), getString(R.string.error_when_download), Toast.LENGTH_SHORT).show();
                Log.d("MyLogs", getClass().getCanonicalName() + e.getMessage());
            }
            return items;
        }

        @Override
        protected void onPreExecute() {
            if (callbacks != null) {
                callbacks.onPreExecute();
            }
        }

        @Override
        protected void onPostExecute(List<? extends BaseItem> list) {
            if (callbacks != null) {
                callbacks.onPostExecute(list);
            }
        }

        @Override
        protected void onProgressUpdate(Void... voids) {
            if (callbacks != null) {

            }
        }

        @Override
        protected void onCancelled() {
            if (callbacks != null) {
                callbacks.onCancelled();
            }
        }
    }
}
