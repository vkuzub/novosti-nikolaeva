package com.vkuzub.nikonews.fragments.workers;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.vkuzub.nikonews.base.BaseItem;
import com.vkuzub.nikonews.model.Article;
import com.vkuzub.nikonews.parser.JsoupHtmlParser;
import com.vkuzub.nikonews.utils.NikoNewsUtils;

import java.io.IOException;

/**
 * Created by Vyacheslav on 30.07.2014.
 */
public class WorkerFragmentArticle extends Fragment {

    private TaskCallbacks callbacks;
    private Task task;

    @Override
    public void onAttach(Activity activity) {
        callbacks = (TaskCallbacks) activity;
        Log.d("MyLogs", getClass().getCanonicalName() + " OnAttach");
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        String articleUrl = getArguments().getString("url");
        task = new Task();
        task.execute(articleUrl);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    private class Task extends AsyncTask<String, Void, Article> {

        @Override
        protected Article doInBackground(String... strings) {
            Article article = null;
            try {
                if (NikoNewsUtils.ACTIVE_SITE.equals(BaseItem.TYPES.NEWSPN)) {
                    article = JsoupHtmlParser.PN.parseArticlePN(strings[0]);
                } else if (NikoNewsUtils.ACTIVE_SITE.equals(BaseItem.TYPES.SITE0512)) {
                    article = JsoupHtmlParser.Site0512.parseArticle0512(strings[0]);
                }
                Log.d("MyLogs", getClass().getCanonicalName() + "doinback, article:" + article);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return article;
        }

        @Override
        protected void onPreExecute() {
            if (callbacks != null) {
                callbacks.onPreExecute();
            }
        }

        @Override
        protected void onPostExecute(Article article) {
            if (callbacks != null) {
                callbacks.onPostExecute(article);
            }
        }

        @Override
        protected void onProgressUpdate(Void... voids) {
            if (callbacks != null) {

            }
        }

        @Override
        protected void onCancelled() {
            if (callbacks != null) {
                callbacks.onCancelled();
            }
        }
    }
}
