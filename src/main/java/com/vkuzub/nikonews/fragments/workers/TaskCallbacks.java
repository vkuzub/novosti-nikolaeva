package com.vkuzub.nikonews.fragments.workers;

/**
 * Created by Vyacheslav on 31.07.2014.
 */
public interface TaskCallbacks {

    void onPreExecute();

    void onProgressUpdate(int percent);

    void onCancelled();

    void onPostExecute(Object object);

}
