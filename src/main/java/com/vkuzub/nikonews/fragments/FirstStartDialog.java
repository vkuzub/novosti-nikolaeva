package com.vkuzub.nikonews.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by Vyacheslav on 14.08.2014.
 */
public class FirstStartDialog extends DialogFragment implements DialogInterface.OnClickListener {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

        adb.setTitle("Не удалось загрузить данные");

        adb.setMessage("Проверьте подключение к интеренету и попробуйте снова");

        adb.setPositiveButton("OK", this);
        setCancelable(false);
        return adb.create();
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case Dialog.BUTTON_POSITIVE:
                getActivity().finish();
                break;
        }
    }

}
