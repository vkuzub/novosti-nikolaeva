package com.vkuzub.nikonews.fragments.lists;

/**
 * Created by Vyacheslav on 26.07.2014.
 */

/**
 * Интерфейс для фрагментов-списков, слушатель нажатия на элемент списка
 */

public interface OnSiteListItemClickListener {
    /**
     * Обработка нажатия на элемент
     *
     * @param url - адрес статьи для парсинга и отображения
     */
    void onItemSelected(String url);
}
