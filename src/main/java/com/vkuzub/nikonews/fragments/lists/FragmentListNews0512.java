package com.vkuzub.nikonews.fragments.lists;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.vkuzub.nikonews.R;
import com.vkuzub.nikonews.adapters.NewsItemAdapter;
import com.vkuzub.nikonews.base.BaseItem;
import com.vkuzub.nikonews.base.BaseItemAdapter;
import com.vkuzub.nikonews.base.BaseListFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Vyacheslav on 23.07.2014.
 */

public final class FragmentListNews0512 extends BaseListFragment {

    protected char parserKey = '0';
    protected final String BASE_URL = "http://www.0512.com.ua/rss";
    protected List<? extends BaseItem> listItems;
    private BaseItem.TYPES type = BaseItem.TYPES.SITE0512;


    @Override
    public List<? extends BaseItem> getListItems() {
        return listItems;
    }

    @Override
    public void fillList(List<? extends BaseItem> items) {
        if (items == null) {
            Toast.makeText(super.activity, super.activity.getResources().getString(R.string.error_when_download), Toast.LENGTH_LONG).show();
            return;
        }
        this.listItems = items;
        BaseItemAdapter adapter = new NewsItemAdapter(super.activity.getApplicationContext(), items);
        setListAdapter(adapter);
    }

    @Override
    protected String createURL() {
        return BASE_URL;
    }

    @Override
    protected BaseItem.TYPES getItemType() {
        return type;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public char getParserKey() {
        return parserKey;
    }

}
