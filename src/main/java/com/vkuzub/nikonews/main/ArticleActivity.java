package com.vkuzub.nikonews.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.vkuzub.nikonews.R;
import com.vkuzub.nikonews.fragments.workers.TaskCallbacks;
import com.vkuzub.nikonews.fragments.workers.WorkerFragmentArticle;
import com.vkuzub.nikonews.model.Article;
import com.vkuzub.nikonews.utils.AsyncImageDownloader;
import com.vkuzub.nikonews.utils.NikoNewsUtils;
import com.vkuzub.nikonews.xml.XmlWorker;

import java.io.IOException;
import java.util.List;

/**
 * Created by Vyacheslav on 18.07.2014.
 */
public class ArticleActivity extends ActionBarActivity implements TaskCallbacks {

    private ProgressDialog progressDialog;

    private WorkerFragmentArticle workerFragment;
    private boolean downloadMoreMedia;

    private TextView tvHeader, tvBody;
    private ImageView ivArticleHeader;
    private LinearLayout llMedia;

    private int displayWidth;
    private String articleUrl;
    private Article article;

    public String getArticleUrl() {
        return articleUrl;
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        articleUrl = getIntent().getStringExtra(NikoNewsUtils.INTENT_URL_EXTRA);
        setContentView(R.layout.fragment_article);
        initWidgets();
        readPreferences();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (!NikoNewsUtils.WORK_MODE.isOnline()) {
            try {
                article = (Article) getIntent().getSerializableExtra(NikoNewsUtils.INTENT_CACHE_ARTICLE);
                articleUrl = article.getArticleUrl();
                Log.d("MyLogs", getClass().getCanonicalName() + " getFromIntentArticle");
                setArticleContent(article);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (savedInstanceState != null) {
                article = (Article) savedInstanceState.getSerializable(NikoNewsUtils.BUNDLE_ARTICLE_TAG);
                Log.d("MyLogs", getClass().getCanonicalName() + " getFromBundleSavedInstance");
                setArticleContent(article);
                return;
            }
        }


        FragmentManager fragmentManager = getSupportFragmentManager();
        workerFragment = (WorkerFragmentArticle) fragmentManager.findFragmentByTag(NikoNewsUtils.TAG_WORKER_FRAGMENT);

        if (workerFragment == null) {
            Bundle args = new Bundle();
            args.putString("url", articleUrl);
            workerFragment = new WorkerFragmentArticle();
            workerFragment.setArguments(args);
            fragmentManager.beginTransaction().add(workerFragment, NikoNewsUtils.TAG_WORKER_FRAGMENT).commit();
        }

    }

    private void initWidgets() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvBody = (TextView) findViewById(R.id.tvArticleBody);
        llMedia = (LinearLayout) findViewById(R.id.llMedia);
        ivArticleHeader = (ImageView) findViewById(R.id.ivArticleHeader);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.article, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!NikoNewsUtils.WORK_MODE.isOnline()) {
            menu.findItem(R.id.menuArticleShare).setVisible(false);
            menu.findItem(R.id.menuArticleWatchOnBrowser).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuArticleShare:

                if (article != null) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.recomend_to_read));
                    shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.recomend_to_read) + " " + article.getTitle() + " " + articleUrl);
                    startActivity(shareIntent);
                } else {
                    Toast.makeText(this, getString(R.string.publication_not_in_cache), Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.menuArticleWatchOnBrowser:

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(articleUrl));
                startActivity(browserIntent);

                break;
            case android.R.id.home:
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPreExecute() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updating));
        progressDialog.show();
    }

    @Override
    public void onPostExecute(Object object) {
        Article article = (Article) object;
        try {
            setArticleContent(article);
            XmlWorker worker = new XmlWorker(getApplicationContext());
            worker.writeArticle(article);
        } catch (NullPointerException e) {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        progressDialog.dismiss();
    }

    @Override
    public void onProgressUpdate(int percent) {

    }

    @Override
    public void onCancelled() {

    }

    void readPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        downloadMoreMedia = sharedPreferences.getBoolean(getResources().getString(R.string.preferences_download_more_media_key), false);
    }

    public void setArticleContent(Article article) {
        this.article = article;
        setHeader(article.getTitle());
        setBody(article.getBody());
        setArticleHeader(article.getHeaderPictureUrl());


        if (downloadMoreMedia && NikoNewsUtils.WORK_MODE.isOnline()) {
            int size = article.getMediaFiles().size();
            if (size > 0) {
                Toast.makeText(getApplicationContext(), getString(R.string.images_downloading) + " " +size, Toast.LENGTH_SHORT).show();
                setMedia(article.getMediaFiles());
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("MyLogs", getClass().getCanonicalName() + "onSaveInstance");
        outState.putSerializable(NikoNewsUtils.BUNDLE_ARTICLE_TAG, article);
    }

    public void setHeader(String header) {
        tvHeader.setText(header);
    }

    public void setBody(String body) {
        tvBody.setText(body);
    }

    public void setArticleHeader(final String url) {
        if (url == null) {
            ivArticleHeader.setImageDrawable(getResources().getDrawable(R.drawable.emptynews));
            return;
        }
        Display display = getWindowManager().getDefaultDisplay();
        //noinspection deprecation
        displayWidth = display.getWidth();
        ivArticleHeader.setTag(url);
        AsyncImageDownloader imageDownloader = new AsyncImageDownloader(displayWidth);
        imageDownloader.execute(ivArticleHeader);
    }

    public void setMedia(List<String> media) {
        ImageView image;
        AsyncImageDownloader imageDownloader;
        for (String mediaUrl : media) {
            image = new ImageView(this);
            image.setTag(mediaUrl);
            imageDownloader = new AsyncImageDownloader(displayWidth);
            imageDownloader.execute(image);
            llMedia.addView(image);
        }
    }

}