package com.vkuzub.nikonews.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.vkuzub.nikonews.R;
import com.vkuzub.nikonews.base.BaseItem;
import com.vkuzub.nikonews.base.BaseListFragment;
import com.vkuzub.nikonews.fragments.FirstStartDialog;
import com.vkuzub.nikonews.fragments.lists.FragmentListNews0512;
import com.vkuzub.nikonews.fragments.lists.FragmentListNewsPN;
import com.vkuzub.nikonews.fragments.lists.OnSiteListItemClickListener;
import com.vkuzub.nikonews.model.Article;
import com.vkuzub.nikonews.utils.NikoNewsUtils;
import com.vkuzub.nikonews.xml.XmlWorker;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vyacheslav on 18.07.2014.
 */
public class MainActivity extends ActionBarActivity implements OnSiteListItemClickListener {


    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private String[] drawerTitles;
    private int drawerPosition = -1;
    private int settingsDrawerPosition = 2;
    private boolean updateOnStart = true;
    private boolean isFirstStart;
    private XmlWorker xmlWorker;
    private boolean isNetworkAvailable;
    private String title;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        readPreferences();
        setWorkMode();
        initDrawer();
        xmlWorker = new XmlWorker(getApplicationContext());

        if (savedInstanceState != null) {
            drawerPosition = savedInstanceState.getInt("position");
            Log.d("MyLogs", getClass().getCanonicalName() + " onRestoreInstancePos: " + drawerPosition);
            return;
        }

        if (NikoNewsUtils.WORK_MODE.isFirstStart()) {
            FirstStartDialog firstStartDialog = new FirstStartDialog();
            firstStartDialog.show(getSupportFragmentManager(), "DialogFirstStart");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!NikoNewsUtils.WORK_MODE.isFirstStart()) {
            if (drawerPosition == -1) {
                selectItem(0);
            } else {
                selectItem(drawerPosition);
            }
        }

        readPreferences();
        setWorkMode();


    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("MyLogs", String.valueOf(getClass().getCanonicalName() + " onRestoreInstance bundle is null " + (savedInstanceState == null)));

        if (savedInstanceState != null) {
            drawerPosition = savedInstanceState.getInt("position");
            Log.d("MyLogs", getClass().getCanonicalName() + " onRestoreInstancePos: " + drawerPosition);
        }
    }

    private void setWorkMode() {
        if (!isNetworkAvailable && isFirstStart) {
            NikoNewsUtils.WORK_MODE = NikoNewsUtils.MODE.FirstStart;
            return;
        }

        if (!updateOnStart || !isNetworkAvailable) {
            NikoNewsUtils.WORK_MODE = NikoNewsUtils.MODE.Offline;
        } else if (updateOnStart && isNetworkAvailable) {
            NikoNewsUtils.WORK_MODE = NikoNewsUtils.MODE.Online;
        }

    }

    /**
     * Метод инициализации NavigationDrawer
     */

    private void initDrawer() {
        drawerTitles = getResources().getStringArray(R.array.drawer_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
//        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, drawerTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.app_name  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                setTitle(title);
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu() ; перерисовка меню
            }

            public void onDrawerOpened(View drawerView) {
                setTitle(getResources().getString(R.string.drawer_open));
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    /**
     * В методе осуществляется проверка открыт ли NavigationDrawer, если открыт - скрывается options Menu
     */
    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (NikoNewsUtils.WORK_MODE.isOnline()) {
            boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
            menu.findItem(R.id.menuListUpdate).setVisible(!drawerOpen);
        } else {
            menu.findItem(R.id.menuListUpdate).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (drawerPosition != settingsDrawerPosition) {
            outState.putInt("position", drawerPosition);
        }
        Log.d("MyLogs", getClass().getCanonicalName() + " onSaveInstancePos: " + drawerPosition);
    }

    /**
     * Обработка нажатий optionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle action buttons
        int tmpPos = drawerPosition;
        switch (item.getItemId()) {
            case R.id.menuListUpdate:
                if (NikoNewsUtils.WORK_MODE.isOnline()) {
                    drawerPosition = -1;
                    selectItem(tmpPos);
                    NikoNewsUtils.isPNCached = false;
                    NikoNewsUtils.isSite0512Cached = false;
                    NikoNewsUtils.deleteCache(getApplicationContext());
                    Toast.makeText(this, getResources().getString(R.string.updating), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.offline_mode), Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(String url) {
        Log.d("MyLogs", getClass().getCanonicalName() + " onItemSelected:" + url);
        if (isNetworkAvailable) {
            Intent intent = new Intent(this, ArticleActivity.class);
            intent.putExtra(NikoNewsUtils.INTENT_URL_EXTRA, url);
            startActivity(intent);
        } else { //чтение из кэша
            String url_key = "";
            Pattern pattern = Pattern.compile(".+/"); //получаем числа статьи

            Matcher matcher = pattern.matcher(url);

            if (matcher.find()) {

                url_key = url.replace(matcher.group(), "");
            }
            Log.d("MyLogs", getClass().getCanonicalName() + " onItemSelectedCache:" + url_key);
            Article article = null;
            try {
                article = xmlWorker.readArticle(url_key);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (article != null) {
                Intent intent = new Intent(this, ArticleActivity.class);
                intent.putExtra(NikoNewsUtils.INTENT_CACHE_ARTICLE, article);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.publication_not_in_cache), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Класс-реализация слушателя для NavigationDrawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    /**
     * Обработка выбраного элемента из NavigationDrawer
     * Замена фрагметов
     *
     * @param position - номер элемента, с нуля
     */

    private void selectItem(int position) {
        BaseListFragment currentList = null;
        if (drawerPosition == position) {
            return;
        }
        drawerPosition = position;
        switch (position) {
            case 0:
                currentList = new FragmentListNewsPN();
                title = getString(R.string.newspn);
                NikoNewsUtils.ACTIVE_SITE = BaseItem.TYPES.NEWSPN;
                break;
            case 1:
                currentList = new FragmentListNews0512();
                title = getString(R.string.news0512);
                NikoNewsUtils.ACTIVE_SITE = BaseItem.TYPES.SITE0512;
                break;
            case 2:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                drawerPosition = -1;
                return;
        }

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.flMain, currentList);
        fragmentTransaction.commit();


        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(drawerTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    void readPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        updateOnStart = sharedPreferences.getBoolean(getResources().getString(R.string.preferences_update_onstart_key), true);
        NikoNewsUtils.AMOUNT_OF_NEWS = Integer.parseInt(sharedPreferences.getString(getString(R.string.preferences_amount_of_news_key), "10"));
        sharedPreferences = getSharedPreferences(NikoNewsUtils.PREFERENCES_NAME, 0);
        isFirstStart = sharedPreferences.getBoolean(getString(R.string.preferences_is_first_start), true);
        NikoNewsUtils.isPNCached = sharedPreferences.getBoolean(getString(R.string.preferences_is_data_cached_pn), false);
        NikoNewsUtils.isSite0512Cached = sharedPreferences.getBoolean(getString(R.string.preferences_is_data_cached_site0125), false);
        isNetworkAvailable = NikoNewsUtils.isNetworkAvailable(this.getApplicationContext());
        Log.d("MyLogs", getClass().getCanonicalName() + " " + String.valueOf("isFirst" + isFirstStart +
                " isCachedPN" + NikoNewsUtils.isPNCached + " isCached0512" + NikoNewsUtils.isSite0512Cached));
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //TODO разобраться
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        if (!isFirstStart) {
            mDrawerToggle.syncState();
//        }
    }

    //TODO разобраться
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences sharedPreferences = getSharedPreferences(NikoNewsUtils.PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (NikoNewsUtils.WORK_MODE.isOnline()) {
            editor.putBoolean(getResources().getString(R.string.preferences_is_first_start), false);
        }
        editor.putBoolean(getResources().getString(R.string.preferences_is_data_cached_pn), NikoNewsUtils.isPNCached);
        editor.putBoolean(getResources().getString(R.string.preferences_is_data_cached_site0125), NikoNewsUtils.isSite0512Cached);
        editor.commit();
    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();
    }
}