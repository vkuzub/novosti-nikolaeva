package com.vkuzub.nikonews.main;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.*;
import android.widget.Toast;
import com.vkuzub.nikonews.R;
import com.vkuzub.nikonews.utils.NikoNewsUtils;

/**
 * Created by Vyacheslav on 28.07.2014.
 */
public class SettingsActivity extends PreferenceActivity {

    private CheckBoxPreference checkBoxUpdateOnStart, checkBoxDownloadMedia;
    private ListPreference listPreferenceAmountOfNews;
    private Preference preferenceSendBug, preferenceAbout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initPreferences();


    }

    @SuppressWarnings("deprecation")
    private void initPreferences() {
        PreferenceScreen preferenceScreen = getPreferenceManager().createPreferenceScreen(this);

        setPreferenceScreen(preferenceScreen);

        PreferenceCategory mainCategory, otherCategory;

        mainCategory = new PreferenceCategory(this);
        mainCategory.setTitle(getString(R.string.preference_main_category));

        checkBoxUpdateOnStart = new CheckBoxPreference(this);
        checkBoxUpdateOnStart.setKey(getResources().getString(R.string.preferences_update_onstart_key));
        checkBoxUpdateOnStart.setTitle(getResources().getString(R.string.preferences_onstartupdate));
        checkBoxUpdateOnStart.setSummary(getResources().getString(R.string.preferences_onstartupdate_summary));
        checkBoxUpdateOnStart.setChecked(true);

        checkBoxDownloadMedia = new CheckBoxPreference(this);
        checkBoxDownloadMedia.setKey(getResources().getString(R.string.preferences_download_more_media_key));
        checkBoxDownloadMedia.setTitle(getResources().getString(R.string.preferences_downloadmoremedia));
        checkBoxDownloadMedia.setSummary(getResources().getString(R.string.preferences_downloadmoremedia_summary));

        listPreferenceAmountOfNews = new ListPreference(this);
        listPreferenceAmountOfNews.setKey(getString(R.string.preferences_amount_of_news_key));
        listPreferenceAmountOfNews.setTitle(getResources().getString(R.string.preferences_amountofnews));
        listPreferenceAmountOfNews.setEntryValues(R.array.preferences_amountofnews_entry_values);
        listPreferenceAmountOfNews.setEntries(R.array.preferences_amountofnews_entries);
        listPreferenceAmountOfNews.setDefaultValue("10");

        otherCategory = new PreferenceCategory(this);
        otherCategory.setTitle(getString(R.string.preference_other_category));

        preferenceSendBug = new Preference(this);
        preferenceSendBug.setTitle(getResources().getString(R.string.preferences_sendmail));
        preferenceSendBug.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                Intent send = new Intent(Intent.ACTION_SENDTO);
                String uriText = "mailto:" + Uri.encode(getString(R.string.preference_sendmail_developer_mail)) +
                        "?subject=" + Uri.encode(getString(R.string.app_name));
                Uri uri = Uri.parse(uriText);
                send.setData(uri);
                startActivity(Intent.createChooser(send, "Send mail..."));

                return true;
            }
        });

        preferenceAbout = new Preference(this);
        preferenceAbout.setTitle(getResources().getString(R.string.preferences_about));
        preferenceAbout.setSummary(getResources().getString(R.string.preferences_about_summary));

        preferenceScreen.addPreference(mainCategory);
        preferenceScreen.addPreference(checkBoxUpdateOnStart);
        preferenceScreen.addPreference(checkBoxDownloadMedia);
        preferenceScreen.addPreference(listPreferenceAmountOfNews);

        preferenceScreen.addPreference(otherCategory);
        preferenceScreen.addPreference(preferenceSendBug);
        preferenceScreen.addPreference(preferenceAbout);

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}