package com.vkuzub.nikonews.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vkuzub.nikonews.R;
import com.vkuzub.nikonews.base.BaseItem;
import com.vkuzub.nikonews.base.BaseItemAdapter;

import java.util.List;

/**
 * Created by Vyacheslav on 19.07.2014.
 */
public class NewsItemAdapter extends BaseItemAdapter {

    public NewsItemAdapter(Context ctx, List<? extends BaseItem> items) {
        super(ctx, items);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = lInflater.inflate(R.layout.listviewitem_news, viewGroup, false);
        }
        BaseItem item = (BaseItem) getItem(i);
        TextView tvNewsItemTime = (TextView) view.findViewById(R.id.tvNewsItemTime);
        TextView tvNewsItem = (TextView) view.findViewById(R.id.tvNewsItem);
        //style
        tvNewsItem.setTextColor(Color.rgb(0, 0, 0));
        tvNewsItemTime.setTextColor(Color.rgb(2, 86, 172));
//        tvNewsItem.setTypeface(tvNewsItem.getTypeface(), Typeface.BOLD);
//        tvNewsItemTime.setTypeface(tvNewsItemTime.getTypeface(), Typeface.BOLD);

        tvNewsItemTime.setText(item.getTime());
        tvNewsItemTime.setBackgroundColor(247247247);
        tvNewsItem.setText(item.getName());

        return view;
    }
}