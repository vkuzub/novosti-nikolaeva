package com.vkuzub.nikonews.parser;

import android.util.Log;
import com.vkuzub.nikonews.base.BaseItem;
import com.vkuzub.nikonews.model.Article;
import com.vkuzub.nikonews.utils.NikoNewsUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Vyacheslav on 18.07.2014.
 * Класс - парсер
 */
public class JsoupHtmlParser {

    public static void main(String[] args) throws IOException {
        System.out.println(Site0512.parseArticle0512("http://www.0512.com.ua/news/632691"));
    }

    public static class PN {

        public static List<BaseItem> parseNewsListForPN(String url) throws IOException {
            boolean isUrlNull = url == null ? true : url.length() == 0;
            if (isUrlNull) {
                return new ArrayList<BaseItem>();
            }

            ArrayList<BaseItem> news = new ArrayList<BaseItem>();
            Document doc;
            Element body;

            doc = Jsoup.connect(url).get();
            body = doc.body();


            Elements ul = body.getElementsByAttributeValueContaining("class", "publications");
            String name;
            String time;
            String itemUrl;

            //определяем, время название и ссылку новости
            for (Element li : ul.get(0).children()) {
                if (news.size() < NikoNewsUtils.AMOUNT_OF_NEWS) {
                    name = li.text().substring(5, li.text().length());
                    time = li.text().substring(0, 5);
                    itemUrl = "http://news.pn" + li.getAllElements().attr("href");

                    news.add(new BaseItem(name, time, itemUrl, null));
                } else {
                    break;
                }
            }
            return news;
        }

        //парсит новости, статьи и блоги. Некоторые картинки не загружает. Пока
        public static Article parseArticlePN(String url) throws IOException {
            boolean isUrlNull = url == null ? true : url.length() == 0;
            if (isUrlNull) {
                return new Article(null, null, null, null, null);
            }
            Document doc;
            StringBuilder text = new StringBuilder();

            doc = Jsoup.connect(url).get();


            Element body = doc.body();
            Elements classContainer = body.getElementsByAttributeValue("class", "container");

            //парсим текст
            for (Element element : classContainer.get(0).getElementsByTag("p")) {
                text.append(element.text()).append("\n\n");
            }

            //парсим картинки
            ArrayList<String> media = new ArrayList<String>();
            for (Element a : classContainer.get(0).getElementsByAttributeValue("rel", "pubphoto")) {
                media.add("http://" + a.attributes().get("href").substring(2));
            }

            String header;
            String articleHeaderUrl = null;

            //парсим название статьи
            header = classContainer.get(0).getElementsByTag("h1").text();

            if (media.size() > 0) {
                articleHeaderUrl = media.get(0);
                media.remove(articleHeaderUrl);
            }


            //        <div class="container">
            //        <div style="margin-top:10px" class="row content">
            //        <article class="ninecol publication lzl hentry">
            //        <header>
            //        <h1 class="entry-title post-title"><a href="/ru/incidents/109226" title="Главный «ополченец» Николаева Янцен намерен люстрировать местных общественников" rel="bookmark">Главный «ополченец» Николаева Янцен намерен люстрировать местных общественников</a></h1>

            String urlnumbers = null;       //id статьи для записи в xml
            Pattern pattern = Pattern.compile("[0-9]+");

            Matcher matcher = pattern.matcher(url);

            while (matcher.find()) {
                urlnumbers = matcher.group();
            }

            return new Article(header, text.toString(), media, articleHeaderUrl, urlnumbers);
        }
    }

    public static class Site0512 {
        public static List<BaseItem> parseNewsListFor0512(String url) throws IOException {
            boolean isUrlNull = url == null ? true : url.length() == 0;
            if (isUrlNull) {
                return new ArrayList<BaseItem>();
            }

            ArrayList<BaseItem> news = new ArrayList<BaseItem>();
            Document doc;
            Element body;

            doc = Jsoup.connect(url).userAgent("Mozilla").get();
            body = doc.body();

            Elements ul = body.getElementsByTag("item");
            String name;
            String time;
            String itemUrl;

            //определяем, время название и ссылку новости
            for (Element li : ul) {
                if (news.size() < NikoNewsUtils.AMOUNT_OF_NEWS) {
                    name = li.getElementsByTag("title").get(0).text();
                    name = name.substring(9, name.length() - 3);
                    itemUrl = li.getElementsByTag("guid").get(0).text();
                    time = li.getElementsByTag("pubDate").get(0).text();

//                Pattern pattern = Pattern.compile("\\d+:\\d.");
                    Pattern pattern = Pattern.compile("\\s\\d.+\\s");
                    Matcher matcher = pattern.matcher(time);

                    if (matcher.find()) {
                        time = matcher.group();
                    }

                    news.add(new BaseItem(name, time, itemUrl, BaseItem.TYPES.SITE0512));
                } else {
                    break;
                }
            }
            return news;
        }

        public static Article parseArticle0512(String url) throws IOException {
            boolean isUrlNull = url == null ? true : url.length() == 0;
            if (isUrlNull) {
                return new Article(null, null, null, null, null);
            }
            Document doc;


            doc = Jsoup.connect(url).userAgent("Mozilla").get();

            Element body = doc.body();
            Elements classContainer = body.getElementsByAttributeValue("class", "conteiner open_news");


            StringBuilder text = new StringBuilder();
            String title = null;
            String articleHeaderUrl = null;
            String urlnumbers = null;       //id статьи для записи в xml
            ArrayList<String> media = new ArrayList<String>(); //картинки в статье

            title = classContainer.get(0).getElementsByTag("h2").get(0).text();

            articleHeaderUrl = classContainer.get(0).getElementsByTag("img").get(0).attr("src");

            Elements classStatic = classContainer.get(0).getElementsByAttributeValue("class", "static");

            for (Element p : classStatic.get(0).getElementsByTag("p")) {

                String ptext = p.text();
                System.out.println(ptext);
                if (ptext.length() != 0) {
                    text.append(ptext + "\n\n");
                } else {
                    try {
                        Elements img = p.getElementsByTag("img");
                        if (img.size() > 0) {
                            media.add(img.attr("src"));
                        }
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        Log.d("MyLogs", ex.toString());
                        ex.printStackTrace();
                    }
                }
            }
//TODO задебажить
            Pattern pattern = Pattern.compile("[0-9]+");

            Matcher matcher = pattern.matcher(url);

            while (matcher.find()) {
                urlnumbers = matcher.group();
            }

            return new Article(title, text.toString(), media, articleHeaderUrl, urlnumbers);
        }
    }

}
